﻿using System.Windows;

namespace _23._2___Cirkel
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

        }


        private void BtnBerekenen_Click(object sender, RoutedEventArgs e)
        {
            lblResultaat.Content = "";
            try
            {
                Cirkel cirkel = new Cirkel(Convert.ToDouble(TxtStraal.Text));
                lblResultaat.Content += cirkel.ToString();
            }
            catch (FormatException)
            {
                MessageBox.Show("U gaf geen numerieke waarde in!", "Foute invoer", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Window_Loaded()
        {

        }
    }
}