﻿namespace _23._2___Cirkel
{
    internal class Cirkel
    {
        //Attributen
        private double _straal;

        //Constructor
        public Cirkel(double straal)
        {
            Straal = straal;
        }

        //Properties
        public double Straal
        {
            get { return _straal; }
            set { _straal = value; }
        }

        //Methoden
        public double BerekenOmtrek()
        {
            return Math.Round((Straal * 2) * Math.PI, 2);
        }
        public double BerekenOppervlakte()
        {
            return Math.Round(Math.PI * Math.Pow(Straal, 2), 2);
        }
        public string FormattedOmtrek()
        {
            return Math.Round(BerekenOmtrek(), 2).ToString();
        }
        public string FormattedOppervlakte()
        {
            return Math.Round(BerekenOppervlakte(), 2).ToString();
        }
        public override string ToString()
        {
            return "Omtrek: " + FormattedOmtrek() + Environment.NewLine + "Oppervlakte: " + FormattedOppervlakte();
        }
    }
}
